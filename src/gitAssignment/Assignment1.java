package gitAssignment;

public class Assignment1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String name = "Lalo";
		String binaryNumber="01101";
		int[] sum = {3,5,7,8,9,10};
		
		recursionPractice rec = new recursionPractice();
		mathEquations mat = new mathEquations();
		
		double result =mat.distanceFormula( 10, 4, 4, 3);
		if(result!=1) {
		System.out.printf("The distance is %.2f", result);
		}else 
			System.out.println("Invalid");
		String slope= mat.getSlope(10, 30, 50, 20);
		System.out.println();
		System.out.println(slope);
		//Should return Lalo backwards
		System.out.println("The reverse of Lalo is "+recursionPractice.reverse(name));
		//Should return 4
		System.out.println("Testing recursive method by taking 2 to the power of 2 which is equal to "+recursionPractice.power(2, 2));
		// Should return 13
		System.out.println("Turning binary number 01101 to decimal is equal to "+recursionPractice.binaryToDecimal(binaryNumber));
		//Should say that 5 is in position 1
		System.out.println("Testing recursive method of Binary Search of the array named sum for the number 5 which is in position "
		+recursionPractice.binarySearch(sum, 5, 0, sum.length));
		//Should display 4
		System.out.println("Testing recursion code for lenght method for lalo which is "+recursionPractice.lenght(name)+
				" letters long");
	}
		

}
